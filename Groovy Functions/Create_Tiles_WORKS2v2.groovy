import qupath.lib.gui.ImageWriterTools
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QPEx
import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP

selectAnnotations();
//Conversion is .2431 between Pixels to microns.  TileSizeMicrons is value squared
runPlugin('qupath.lib.algorithms.TilerPlugin', '{"tileSizeMicrons": 72.6869,  "trimToROI": true,  "makeAnnotations": true,  "removeParentAnnotation": true}');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 0.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 1.5,  "minAreaMicrons": 10.0,  "maxAreaMicrons": 400.0,  "threshold": 0.2,  "maxBackground": 2.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 5.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');

runClassifier("G:\\Studies\\Tumor vs Stroma Prostate\\classifiers\\ed3.qpclassifier");

String Imagename = getProjectEntry().getImageName()
def path = "G:\\Studies\\Tumor vs Stroma Prostate\\QuTiles\\" + Imagename
// Make all of the paths where tiles will go, add paths and create directories accordingly
def path2 = QPEx.buildFilePath(path, "50-89% Tumor")
QPEx.mkdirs(path2)
def path3 = QPEx.buildFilePath(path, "50-89% Stroma")
QPEx.mkdirs(path3)
def path4 = QPEx.buildFilePath(path, "90-100% Tumor")
QPEx.mkdirs(path4)
def path5 = QPEx.buildFilePath(path, "90-100 Stroma")
QPEx.mkdirs(path5)
def path6 = QPEx.buildFilePath(path, "No cells")
QPEx.mkdirs(path6)


//get some data before getting into the loop
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()

def filename = server.getShortServerName()

//counter for debugging purposes
i = 1

//loop through detections
for (annotation in getAnnotationObjects()) 
{
    
    //make roi the annotation, then find the centroids of that annotation to become the file name eventually
    roi = annotation.getROI()
    int cx = roi.getCentroidX()
    int cy = roi.getCentroidY()
    def request = RegionRequest.createInstance(imageData.getServerPath(), 
        1, roi)
        
    //Look at all detections within a SPECIFIED annotation, not the WHOLE annotation...only commands I could get to work.
    def TumorDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Tumor"}
    def StromaDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Stroma"}
    
    
    //can change what classes you want here, need to match up there   ^^    
    tumorClass = getPathClass("Tumor")
    stromaClass = getPathClass("Stroma")
    // reset counters
    
    nTumor = 0
    nStroma = 0
    
    //counting numbers of each class
        for (detection in TumorDetections) {           
                  nTumor++
                                           }  
    for(detection in StromaDetections)    {
        nStroma++       
                                          } 
//ratio of the cells, may be used for binning in folders?   
int TotalCell = 0
TotalCell = nStroma + nTumor                                                        
double ratio = 0
if (nStroma > 0 || nTumor > 0)  {
ratio = (nTumor/TotalCell)
                                 }

//string for the file name, has the x and y coodinates we pulled from the annotation cx,cy
String tilename = String.format("%s %s_%s.jpg",Imagename, cx,cy)

print(ratio)
//these series of if statements just toss a tile into a certain folder in path with the coordinates depending on whatever ratios you want
    if (ratio > 0.5 && ratio < 0.9) {
        def dir = new File(path2)
        ImageWriterTools.writeImageRegion(server, request, path2 + "/" + tilename);
        print("wrote " + tilename)
    
        
                                    }
   if (ratio < 0.5 && ratio > 0.1){
        def dir = new File(path3)
        ImageWriterTools.writeImageRegion(server, request, path3 + "/" + tilename);
        print("wrote " + tilename)
                                    }

    if (ratio  > 0.9){
       
    
       
        def dir = new File(path4)
        ImageWriterTools.writeImageRegion(server, request, path4 + "/" + tilename);
        print("wrote " + tilename)
                                    }
    if (ratio < 0.1 && ratio > 0){
       
    
       
        def dir = new File(path5)
        ImageWriterTools.writeImageRegion(server, request, path5 + "/" + tilename);
        print("wrote " + tilename)
                                    }
                                    
    if (ratio == 0){
       
    
       
        def dir = new File(path6)
        ImageWriterTools.writeImageRegion(server, request, path6 + "/" + tilename);
        print("wrote " + tilename)
                                    }
                                    
    
   
    
       
  
}
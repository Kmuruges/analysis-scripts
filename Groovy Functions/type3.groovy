setImageType('BRIGHTFIELD_H_DAB')
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.65561 0.68482 0.31813 ", "Stain 2" : "DAB", "Values 2" : "0.34892 0.85162 0.39114 ", "Background" : " 255 255 255 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 0.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 1.7,  "minAreaMicrons": 18.0,  "maxAreaMicrons": 75.0,  "threshold": 0.43,  "maxBackground": 2.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 4.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');


setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "Semma Pixels2", "Stain 1" : "Hematoxylin", "Values 1" : "0.69381 0.61155 0.38031 ", "Stain 2" : "DAB", "Values 2" : "0.22312 0.4936 0.84058 ", "Background" : " 252 252 249 "}');
selectAnnotations()
mergeSelectedAnnotations()
selectAnnotations()
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 1,  "gaussianSigmaMicrons": 0.2,  "thresholdStain1": 0.02,  "thresholdStain2": 0.7,  "addSummaryMeasurements": true}');

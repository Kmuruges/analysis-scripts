setImageType('BRIGHTFIELD_H_DAB');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 242,  "requestedPixelSizeMicrons": 20.0,  "minAreaMicrons": 500000.0,  "maxHoleAreaMicrons": 20000.0,  "darkBackground": false,  "smoothImage": false,  "medianCleanup": false,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": true}');
setImageType('BRIGHTFIELD_H_E');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 15.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 2.0,  "minAreaMicrons": 10.0,  "maxAreaMicrons": 400.0,  "threshold": 0.2,  "maxBackground": 2.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 10.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');

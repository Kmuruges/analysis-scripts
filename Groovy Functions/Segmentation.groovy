setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.23521 0.53265 0.81299 ", "Stain 2" : "DAB", "Values 2" : "0.64254 0.62116 0.44867 ", "Background" : " 255 255 255 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 9.0,  "medianRadiusMicrons": 2.0,  "sigmaMicrons": 2.0,  "minAreaMicrons": 100.0,  "maxAreaMicrons": 1000.0,  "threshold": 0.1,  "maxBackground": 3.0,  "watershedPostProcess": false,  "excludeDAB": false,  "cellExpansionMicrons": 10.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');

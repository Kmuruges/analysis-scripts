
//setImageType('BRIGHTFIELD_H_E');
//setColorDeconvolutionStains('{"Name" : "H&E default", "Stain 1" : "Hematoxylin", "Values 1" : "0.65111 0.70119 0.29049 ", "Stain 2" : "Eosin", "Values 2" : "0.2159 0.8012 0.5581 ", "Background" : " 255 255 255 "}');
//createSelectAllObject(true);
//setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.62849 0.71646 0.30279 ", "Stain 2" : "DAB", "Values 2" : "0.50932 0.80003 0.31708 ", "Background" : " 255 255 255 "}');
//selectAnnotations();
//runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "backgroundRadius": 0.0,  "medianRadius": 0.0,  "sigma": 3.0,  "minArea": 10.0,  "maxArea": 1500.0,  "threshold": 0.8,  "maxBackground": 2.0,  "watershedPostProcess": true,  "excludeDAB": false,  "cellExpansion": 5.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');
//runClassifier("G:\\Studies\\Sakura QC Check\\Testq\\classifiers\\Working.qpclassifier");//


import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP
import java.awt.image.BufferedImage

/*
 * Adjustable parameters
 */
int tileWidthPixels = 150 // Width of (final) output tile in pixels
int tileHeightPixels = tileWidthPixels // Width of (final) output tile in pixels
double downsample = 1      // Downsampling used when extracting tiles
String format = "png"       // Format of the output image - TIFF or ZIP is best for ImageJ to preserve pixel sizes
String dirOutput = "G:\\Studies\\Positive Fungus\\centeredFungalTiles\\"     // BE SURE TO ADD AN OUTPUT DIRECTORY HERE!!!

int maxErrors = 20          // Maximum number of errors... to avoid trying something doomed forever
int minImageDimension = 150  // If a tile will have a width or height < minImageDimension, it will be skipped
                            // This is needed to avoid trying to read/write images that are too tiny to be useful (and may even cause errors)

//-------------------------------------------------------

/*
 * Processing
 */

// Check we have an output directory
if (dirOutput == null) {
    println("Be sure to set the 'dirOutput' variable!")
    return
}

// Initialize error counter
int nErrors = 0

// Get the image server
ImageServer<BufferedImage> serverOriginal = QP.getCurrentImageData().getServer()

// Get an ImagePlus server
ImagePlusServer server = ImagePlusServerBuilder.ensureImagePlusWholeSlideServer(serverOriginal)

// Ensure convert the format to a file extension
String ext
if (format.startsWith("."))
    ext = format.substring(1).toLowerCase()
else
    ext = format.toLowerCase()

// Extract useful variables
String path = server.getPath()
String serverName = serverOriginal.getShortServerName()
int tileWidth = tileWidthPixels * downsample
int tileHeight = tileHeightPixels * downsample

// Loop through the image - including z-slices (even though there's normally only one...)
int counter = 0;
z=0
//loop through detections
   for (object in getDetectionObjects()) {
      
    // Get the ROI
    def roi = object.getROI()
    if (roi == null)
        continue
        
        int x1 = (roi.getCentroidX()) 
        int y1 = (roi.getCentroidY())
        
       //tile will be placed @ upper left corner, since tile is 150x150, this will make the tile centroid match the obj centroid
        int xi = x1 - (0.5*tileWidthPixels);
        int yi = y1 - (0.5*tileWidthPixels); //note Qupath puts upperleft of slide as (0,0)


            // Create request
            RegionRequest request = RegionRequest.createInstance(dirOutput, downsample, xi, yi, tileWidth, tileHeight, z, 0)
            
                     
            // Surround with try/catch in case the server gives us trouble
            try {
                // Read the image region
                ImagePlus imp = server.readImagePlusRegion(request).getImage(false)
                // Get a suitable file name
                String name = String.format("%s (d=%.2f, x=%d, y=%d, w=%d, h=%d, z=%d).%s", serverName, downsample, xi, yi, tileWidth, tileHeight, z, ext)
                // Create an output file
                File file = new File(dirOutput, name)
                // Save the image
                IJ.save(imp, file.getAbsolutePath())
                // Print progress
        //ImageWriterTools.writeImageRegionWithOverlay(imageData, overlayOptions, imp, fileImageWithOverlay.getAbsolutePath())
                counter++
                println("Written tile " + counter + " to " + file.getAbsolutePath())
            } catch (Exception e) {
                // Check if we have had a sufficient number of errors to just give up
                nErrors++;
                if (nErrors > maxErrors) {
                    println("Maximum number of errors exceeded - aborting...")
                    return
                }
                e.printStackTrace()
            }
            
            }
        
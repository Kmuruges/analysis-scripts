setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.65111 0.70119 0.29049 ", "Stain 2" : "DAB", "Values 2" : "0.26917 0.56824 0.77759 ", "Background" : " 255 255 255 "}');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 240,  "requestedPixelSizeMicrons": 5.0,  "minAreaMicrons": 1000000.0,  "maxHoleAreaMicrons": 10000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": false}');
for (annotation in getAnnotationObjects())
    annotation.setLocked(false)   
fireHierarchyUpdate()
selectAnnotations()
runPlugin('qupath.imagej.superpixels.SLICSuperpixelsPlugin', '{"sigmaMicrons": 5.0,  "spacingMicrons": 50.0,  "maxIterations": 10,  "regularization": 0.25,  "adaptRegularization": false,  "useDeconvolved": false}');
selectDetections();
runPlugin('qupath.lib.algorithms.IntensityFeaturesPlugin', '{"pixelSizeMicrons": 2.0,  "region": "ROI",  "tileSizeMicrons": 25.0,  "colorOD": true,  "colorStain1": true,  "colorStain2": false,  "colorStain3": false,  "colorRed": true,  "colorGreen": false,  "colorBlue": true,  "colorHue": true,  "colorSaturation": true,  "colorBrightness": true,  "doMean": true,  "doStdDev": false,  "doMinMax": false,  "doMedian": false,  "doHaralick": false,  "haralickDistance": 1,  "haralickBins": 32}');
selectDetections();
runPlugin('qupath.lib.algorithms.LocalBinaryPatternsPlugin', '{"magnification": 1.0,  "stainChoice": "H-DAB",  "tileSizeMicrons": 20.0,  "includeStats": true,  "doCircular": true}');
runClassifier("G:\\17-305 Concortis\\QuConcortisSTI\\classifiers\\Positive region detection.qpclassifier")
selectAnnotations()
runPlugin('qupath.lib.analysis.objects.TileClassificationsToAnnotationsPlugin', '{"pathClass": "All classes",  "deleteTiles": true,  "clearAnnotations": true,  "splitAnnotations": false}');
selectObjects { p -> p.getPathClass() == null }
clearSelectedObjects();

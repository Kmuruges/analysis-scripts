//THE STRATEGY HERE IS TO AGGRESSIVELY ANALYZE ALL TISSUE REGIONS TO CHURN OUT AS MUCH DATA AS POSSIBLE.
//since we are generating so much data we can afford to be more selective and choose only gold standard types of data.
 

//other code locations by Krishna: SSD Analysis: G:\QupathCode\Macro
//other images: C:\Users\RevealBio\Desktop\Ryan QuPath Stuff\Generated Tiles

//splits whole slide image into ImageJ-written output images; ImageJ allows for storing of the metadata such as pixel sizes/coords

import qupath.lib.gui.ImageWriterTools
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QPEx
import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP

//the below libraries are used for exporting annotation overlays
//this code generates thumbnail images with the various bounding boxes (rectangles, polygons) that you drew on top of the tissue
//just a visual representation of each image that was drawn (kind of like my stitched images)
import qupath.lib.gui.ImageWriterTools
import qupath.lib.gui.QuPathGUI
import qupath.lib.gui.viewer.OverlayOptions
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QPEx

double requestedPixelSize = 5

//used the color deconvolution stains that Krishna established (copied it from the script-->hematoxylin counterstains for purple-->dark purple is dark, light purple is light; eosin stains for pink,
//dark pink is dark, light pink is light
setImageType('BRIGHTFIELD_H_E');
setColorDeconvolutionStains('{"Name" : "H&E default", "Stain 1" : "Hematoxylin", "Values 1" : "0.69132 0.69345 0.20299 ", "Stain 2" : "Eosin", "Values 2" : "0.32355 0.83444 0.44612 ", "Background" : " 213 105 189 "}');

//we do tissue detection here to create annotations of the tissue
//min area 10mil; max fill area 500K; threshold 245; exclude on boundaries, don't make single annotation
//runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 245,  "requestedPixelSizeMicrons": 0.0,  "minAreaMicrons": 1.0E7,  "maxHoleAreaMicrons": 500000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": true,  "singleAnnotation": false}');

//no need for this
//this is for merging all the tiles if the slides were already tiled by _RyanUpdatedCode prior. That way we can get accurate bounding box values.
//selectAnnotations();
//mergeSelectedAnnotations();

//before running this script need to have an annotation
// go to Analyze-->Preprocessing-->Simple Tissue detection. The large tissues that are detected
//will then be tiled.

//these are the default workflow settings that we have used (no need to set brightfield or color deconv stains--these are defaults) 
//but use .algorithms.TilerPlugin which is derived from https://groups.google.com/forum/#!topic/qupath-users/rR5Abm97dyk
selectAnnotations();

//if the tiles were formed prior (because this code was ran on the same project many times) then merge all the tiles together, and start the retiling
//again (this gets the proper coordinates for the bounding box)
//mergeSelectedAnnotations();

//https://gist.github.com/petebankhead/a473b817eac36caabdffc21daf93231d
//creates a folder for the project name
//this was Krishna's file path:"X:\\Studies\\Tumor vs Stroma Prostate\\QuTiles\\" + Imagename
//I will use a default file path to place the tiles in
String Imagename = getProjectEntry().getImageName()
//def path = "C:\\Users\\rtam\\Desktop\\QuPath Items\\Generated Tiles\\"+ Imagename
//def path = "T:\\Ryan QuPath Tiles\\"+ Imagename
def path = "X:\\SERVERPC_TILES"
QPEx.mkdirs(path)

//after getting all the annotations get the bounding box (all the vertices) of each of those annotations (these annotations drawn around
//the tissue) and put into a textfile
//gets the coordinates for every tile to be saved
//def pathCoords="C:\\Users\\rtam\\Desktop\\QuPath Items\\Generated Tiles"
File fileOutput = new File(resolvePath(path));	
fileOutput = new File(fileOutput, "coordinates"+Imagename+".txt");
PrintWriter writer = new PrintWriter(fileOutput);

for (annotation in getAnnotationObjects()) 
{
    roi = annotation.getROI()
    print roi.getPolygonPoints()
    String vertices = roi.getPolygonPoints()
    //def path = "G:\\18-135 eFFECTOR\\"
    writer.println(vertices);
}

writer.close();

//NOW DO THE TILING OF THE IMAGES
//the TilerPlugin does all the separation into tiles of 73X73 micron (or 299X299 pixel after our conversion). TrimToROI gets rid of all the edge tiles that do not fit that region. Each tile will have the annotations of the cells
//from WatershedCellDetect (so we can count the number of cells from within) as well as
// its classification of tumor, stroma, immune, etc. in RunClassifier; removeParentAnnotation means it gets rid of the larger annotation of the entire tissue, so it only gets the tile annotations
//Conversion is .2431 between Pixels to microns.  TileSizeMicrons is value squared
//set trimToROI false to only get 299X299 tiles with whitespace in them (but deep learning probably does not need the whitespace)
//I can feed images through my Python processing and omit the smaller images
runPlugin('qupath.lib.algorithms.TilerPlugin', '{"tileSizeMicrons": 72.6869,  "trimToROI": false,  "makeAnnotations": true,  "removeParentAnnotation": true}');
selectAnnotations();

//note for Cell Detection; my training model used a threshold of 0.2 to get training data
//but here we set thresold to 0.1 because in some data the cell nuclei intensity are dimmer so we need to account for those
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 0.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 1.5,  "minAreaMicrons": 10.0,  "maxAreaMicrons": 400.0,  "threshold": 0.1,  "maxBackground": 2.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 5.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');

//I WILL MODIFY THIS CLASSIFIER AND STRENGTHEN IT OVER TIME
//THE CLASSIFIER: from \\Analysispc2\v\Tumor vs Stroma\5 Class Training Tiles, 
//High Grade, LowGrade, Immune, Stroma, are all classes
//C:\Users\rtam\Documents\Cureline Stuff\classifiers\ed3.qpclassifier
//but I put it in my local drive so I can run from home: C:\Users\rtam\Documents\Cureline Stuff\classifiers\ed3.qpclassifier
//runClassifier("C:\\Users\\rtam\\Desktop\\QuPath Items\\Classifiers\\ed3.qpclassifier");

//runClassifier("C:\\Users\\rtam\\Desktop\\QuPath Items\\Cureline2\\classifiers\\TumorImmuneStromaClass.qpclassifier");
//runClassifier("C:\\Users\\rtam\\Desktop\\QuPath Items\\Cureline3\\classifiers\\TumStromImmuneClassifier.qpclassifier");
runClassifier("X:\\Ryan scripts\\TumStromImmuneClassifierV2.qpclassifier");


//https://github.com/qupath/qupath/issues/125
//import qupath.lib.scripting.QPEx and QPEx.buildFilePath dynamically creates target folder for the results
// Make all of the paths where tiles will go within the Project, add paths and the necessary directories accordingly
//also create a necrosis folder where if getDetectionObjects()<5 or 10 within the tile, we can put it in the necrosis folder and analyze by hand (maybe we can separate by intensity)
//previously necrosis was unlabeled and the difference between total tissue area and the tumor/stroma areas. But our way here is better.
def path2 = QPEx.buildFilePath(path, "Tumor")
QPEx.mkdirs(path2)
def path3 = QPEx.buildFilePath(path, "Stroma")
QPEx.mkdirs(path3)
def path4 = QPEx.buildFilePath(path, "Necrosis")
QPEx.mkdirs(path4)
def path5 = QPEx.buildFilePath(path, "Immune")
QPEx.mkdirs(path5)

//creates a secondary pathway
def path6 = QPEx.buildFilePath(path, "Annotated Thumbnails of Slides")
QPEx.mkdirs(path6)

//also create a pathway for halfway cases
def pathHalf = QPEx.buildFilePath(path, "Halfway Cases")
QPEx.mkdirs(pathHalf)

//get some data before getting into the loop
//https://groups.google.com/forum/#!topic/qupath-users/pBEmidv0Wow
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()
def filename = server.getShortServerName()


//creates the annotation overlay on the thumbnail slide image (Krishna figured out the downsample value)
def overlayOptions = QuPathGUI.getInstance() == null ? new OverlayOptions() : QuPathGUI.getInstance().getViewer().getOverlayOptions()

double downsample = requestedPixelSize / server.getAveragedPixelSizeMicrons()
def requestDownsample = RegionRequest.createInstance(imageData.getServerPath(), downsample, 0, 0, server.getWidth(), server.getHeight())

//puts the image in the annotated thumbnails of slides (do this before we get all of the tiles; because the whole slide is separate from individ tiles)
def dir = new File(path6)
def fileImageWithOverlay = new File(dir, filename + "-Annotation.jpg")
ImageWriterTools.writeImageRegionWithOverlay(imageData, overlayOptions, requestDownsample, fileImageWithOverlay.getAbsolutePath())

//loop through each of the annotated tiles (polygons); again, not parent annotation/tissue because disabled
for (annotation in getAnnotationObjects()) 
{
    int enoughDetections=1 //initially assumed to have enough detections
    
    //make roi the annotation, then find the centroids of that annotation to become the file name eventually
    roi = annotation.getROI()
    int cx = roi.getCentroidX()
    int cy = roi.getCentroidY()
    
    //RegionRequest request = RegionRequest.createInstance(path, downsample, xi, yi, wi, hi, z, 0)
    //we get the image path, no downsampling and ROI accounts for dimensions
    def request = RegionRequest.createInstance(imageData.getServerPath(), 1, roi)
        
    //Look at all detections within a SPECIFIED annotation, not the WHOLE annotation...only commands I could get to work.
    //Detection objects within the polygon or area can be tumor or stroma (we also want to register immune detections because there are many that are small and dense close to tumors, and we don't want to skew
    //the ratios of tumor or stroma
    //ie below gets annotations of a certain size https://gist.github.com/Svidro/8f9c06e2c8bcae214cdd7aa9afe57c50
    //def smallAnnotations = getAnnotationObjects().findAll {it.getROI().getArea() < 400000}
    def TumorDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Tumor"}
    def StromaDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Stroma"}
    def ImmuneDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Immune cells"}
    
    
    //can change what classes you want here, need to match up there   ^^    
    tumorClass = getPathClass("Tumor")
    stromaClass = getPathClass("Stroma")
    
    // reset counters for every tile, so we can account number of tumor, stroma, and immune cells
    nTumor = 0
    nStroma = 0
    nImmune = 0
    
    //counting numbers of each class
    for (detection in TumorDetections) 
    {           
       nTumor++
    }  
    
    for(detection in StromaDetections)    
    {
       nStroma++       
    } 
    
    for(detection in ImmuneDetections)    
    {
       nImmune++       
    }     
    

    //ratio of the cells, to bin in folders (add Immune to prevent skew) 
    int TotalCell = 0
    TotalCell = nStroma + nTumor + nImmune                                                      
    double ratio = 0
    print("Total cells are " + TotalCell)
    
    //if less than 2 cells in a tile
    if (TotalCell<2)     
    {
       print("Went here")
       enoughDetections=0 //not enough detections, so would be placed in necrosis bin
    }
    
    //initially set tumor and stroma ratio to low values (need to define this so it takes in a standard value)
    tumorRatio=0
    stromaRatio=0
    immuneRatio=0
    
    if (nStroma > 0 || nTumor > 0 || nImmune>0)  
    {
        tumorRatio = (nTumor/TotalCell)
        stromaRatio = (nStroma/TotalCell)
        immuneRatio = (nImmune/TotalCell)
    }
    
    //string for the file name, has the x and y coodinates we pulled from the annotation cx,cy
    String tilename = String.format("%s %s_%s TSI %s_%s_%s.jpg",Imagename, cx,cy,nTumor,nStroma,nImmune)
    
    //prints out the tumorRatio
    print(tumorRatio)
    
   //prints out stromaRatio
    print(stromaRatio)
    
    
   //these series of if statements just toss a tile into a certain folder in path with the coordinates depending on whatever ratios you want
   //highest priority is if there are even enough detections in the tile; if not enough detections, we immediately put to the necrosis folder
   //this code is now configured to put tiles as necrosis if not enough tissue exists
        
    if (enoughDetections==0)
    {
        def dir4 = new File(path4)
        ImageWriterTools.writeImageRegion(server, request, path4 + "/" + tilename);
        print("Necrosis wrote " + tilename)
    }
    //put halfway conditionals here
    else if (tumorRatio > 0.5 && nTumor>15) //if over 50% of the seen cells in the tile are labeled tumor; greater than 15 tumor
    {
        //if there are a number of stroma or immune close to the tumor case, lump it to halfway case
        //avoid if greater than 15 immune
        if (nTumor<nStroma*2 || nTumor<nImmune*2 || nImmune>15)
        {
            def dirHalf = new File(pathHalf)
            ImageWriterTools.writeImageRegion(server, request, pathHalf + "/" + tilename);
            print("Halfway case wrote " + tilename)
        } 
        else
        {
            def dir2 = new File(path2)
            ImageWriterTools.writeImageRegion(server, request, path2 + "/" + tilename);
            print("Tumor wrote " + tilename)
        }
    }
   else if (stromaRatio > 0.5 && nStroma>6)
   {
        //if there are a number of tumor or immune close to the stroma case, lump it to halfway case
        if (nStroma<nTumor*2 || nStroma<nImmune*2)
        {
            def dirHalf = new File(pathHalf)
            ImageWriterTools.writeImageRegion(server, request, pathHalf + "/" + tilename);
            print("Halfway case wrote " + tilename)
        } 
        else
        {
            def dir3 = new File(path3)
            ImageWriterTools.writeImageRegion(server, request, path3 + "/" + tilename);
            print("Stroma wrote " + tilename)
        }
   }
    else if (immuneRatio > 0.5 && nImmune>20) //be more aggressive, make it at least 20 immune cells
    {
        //if there are a number of tumor or stroma close to the immune case, lump it to halfway case
        //also omit cases of at least 10 tumor cells to prevent those cases
        if (nImmune<nTumor*2 || nImmune<nStroma*2 || nTumor >9)
        {
            def dirHalf = new File(pathHalf)
            ImageWriterTools.writeImageRegion(server, request, pathHalf + "/" + tilename);
            print("Halfway case wrote " + tilename)
        } 
        else
        {
            def dir5 = new File(path5)
            ImageWriterTools.writeImageRegion(server, request, path5 + "/" + tilename);
            print("Immune wrote " + tilename)
        }
    }
  
}
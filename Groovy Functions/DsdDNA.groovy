setImageType('FLUORESCENCE');
runPlugin('qupath.imagej.detect.nuclei.PositiveCellDetection', '{"detectionImageFluorescence": 3,  "backgroundRadius": 0.0,  "medianRadius": 0.0,  "sigma": 3.0,  "minArea": 10.0,  "maxArea": 1000.0,  "threshold": 50.0,  "watershedPostProcess": true,  "cellExpansion": 3.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true,  "thresholdCompartment": "Nucleus: DAB OD mean",  "thresholdPositive1": 0.2,  "thresholdPositive2": 0.4,  "thresholdPositive3": 0.6,  "singleThreshold": true}');
resetDetectionClassifications();

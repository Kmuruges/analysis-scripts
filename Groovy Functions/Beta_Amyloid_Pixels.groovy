setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.94241 0.33057 0.05086 ", "Stain 2" : "DAB", "Values 2" : "0.55473 0.59582 0.58075 ", "Background" : " 253 253 250 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 4,  "gaussianSigmaMicrons": 2.0,  "thresholdStain1": 0.5,  "thresholdStain2": 0.9,  "addSummaryMeasurements": true}');

setImageType('FLUORESCENCE');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageFluorescence": 3,  "requestedPixelSizeMicrons": 0.24,  "backgroundRadiusMicrons": 0.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 2.5,  "minAreaMicrons": 15.0,  "maxAreaMicrons": 250.0,  "threshold": 75.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 5.0,  "includeNuclei": true,  "smoothBoundaries": false,  "makeMeasurements": true}');

import qupath.lib.objects.classes.PathClass
import qupath.lib.objects.classes.PathClassFactory


def key = "Cytoplasm: DAB OD mean"
)


for (cell in getCellObjects()){
   def value = cell.getMeasurementList().getMeasurementValue(key)
	if (value > 0.3 && value > 120) 
	    cell.setPathClass(Excluded)
	else 
	    cell.setPathClass(Excluded2)
	
}
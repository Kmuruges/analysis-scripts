selectAnnotations();
//Conversion is .2431 between Pixels to microns.  TileSizeMicrons is value squared
runPlugin('qupath.lib.algorithms.TilerPlugin', '{"tileSizeMicrons": 18.2325,  "trimToROI": true,  "makeAnnotations": true,  "removeParentAnnotation": true}');


String Imagename = getProjectEntry().getImageName()
def path = "G:\\Studies\\Compass Pos\\" + Imagename
// Make all of the paths where tiles will go, add paths and create directories accordingly
def path2 = QPEx.buildFilePath(path, "Positive75")
QPEx.mkdirs(path2)




//get some data before getting into the loop
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()

def filename = server.getShortServerName()

//counter for debugging purposes
i = 1
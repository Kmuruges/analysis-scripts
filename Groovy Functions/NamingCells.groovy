
for (annotation in getAnnotationObjects())
{
   def String name = annotation.getName()
   def descendantDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() )}   

       for (detection in descendantDetections)
           { 
           detection.setName(detection.getParent().getName())
           }

}
import qupath.lib.gui.ImageWriterTools
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QPEx
import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP


def path = "G:\\Studies\\Tumor vs Stroma Prostate\\QuTiles\\"
def path2 = QPEx.buildFilePath("G:\\Studies\\Tumor vs Stroma Prostate\\QuTiles\\", "Tumor")
QPEx.mkdirs(path2)
def path3 = QPEx.buildFilePath("G:\\Studies\\Tumor vs Stroma Prostate\\QuTiles\\", "Stroma")
QPEx.mkdirs(path3)
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()

def filename = server.getShortServerName()

i = 1

for (annotation in getAnnotationObjects()) {

    roi = annotation.getROI()
    double cx = roi.getCentroidX()
    double cy = roi.getCentroidY()
    def request = RegionRequest.createInstance(imageData.getServerPath(), 
        1, roi)
    def TumorDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Tumor"}
    def StromaDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Stroma"}
           
    tumorClass = getPathClass("Tumor")
    stromaClass = getPathClass("Stroma")
    nTumor = 0
    nStroma = 0
        for (detection in TumorDetections) {           
                  nTumor++
                                           }  
    for(detection in StromaDetections)    {
        nStroma++       
                                          }                  

String tilename = String.format("%s_%s.jpg", cx,cy)

    if (nTumor > 0 && nStroma == 0) {
    
   
    
       
        def dir = new File(path2)
        ImageWriterTools.writeImageRegion(server, request, path2 + "/" + tilename);
        print("wrote " + tilename)
    
        
                                    }
   if (nTumor == 0 && nStroma > 0){
       
    
       
        def dir = new File(path3)
        ImageWriterTools.writeImageRegion(server, request, path3 + "/" + tilename);
        print("wrote " + tilename)
                                    }
}
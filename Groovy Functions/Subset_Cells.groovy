tumorClass = getPathClass("Tumor")
nTumor = 0
for (detection in getDetectionObjects()) {
    pathClass = detection.getPathClass()
    if (pathClass == tumorClass)
      nTumor++
}
print(nTumor)

stromaClass = getPathClass("Stroma")
nStroma = 0
for (detection in getDetectionObjects()) {
    pathClass = detection.getPathClass()
    if (pathClass == stromaClass)
      nStroma++
}
print(nStroma)


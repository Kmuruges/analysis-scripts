setImageType('BRIGHTFIELD_H_DAB');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 248,  "requestedPixelSizeMicrons": 0.25,  "minAreaMicrons": 3000000.0,  "maxHoleAreaMicrons": 50000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": false}');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.68531 0.60321 0.40804 ", "Stain 2" : "DAB", "Values 2" : "0.29895 0.52839 0.79463 ", "Background" : " 255 255 255 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 4,  "gaussianSigmaMicrons": 1.5,  "thresholdStain1": 0.9,  "thresholdStain2": 0.15,  "addSummaryMeasurements": true}');
selectAnnotations();
runPlugin('qupath.lib.algorithms.IntensityFeaturesPlugin', '{"pixelSizeMicrons": 2.0,  "region": "ROI",  "tileSizeMicrons": 25.0,  "colorOD": false,  "colorStain1": false,  "colorStain2": true,  "colorStain3": false,  "colorRed": false,  "colorGreen": false,  "colorBlue": false,  "colorHue": false,  "colorSaturation": false,  "colorBrightness": false,  "doMean": true,  "doStdDev": false,  "doMinMax": false,  "doMedian": false,  "doHaralick": false,  "haralickDistance": 1,  "haralickBins": 32}');

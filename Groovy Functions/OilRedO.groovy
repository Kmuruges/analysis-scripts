setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.55828 0.71475 0.42125 ", "Stain 2" : "DAB", "Values 2" : "0.99318 0.11246 0.03067 ", "Background" : " 255 255 255 "}');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 234,  "requestedPixelSizeMicrons": 0.25,  "minAreaMicrons": 300000.0,  "maxHoleAreaMicrons": 10000.0,  "darkBackground": false,  "smoothImage": false,  "medianCleanup": false,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": true}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 2,  "gaussianSigmaMicrons": 0.2,  "thresholdStain1": 0.35,  "thresholdStain2": 8.0,  "addSummaryMeasurements": true}');

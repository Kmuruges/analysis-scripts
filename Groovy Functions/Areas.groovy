setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.81588 0.52797 0.23577 ", "Stain 2" : "DAB", "Values 2" : "0.16896 0.82409 0.54068 ", "Background" : " 247 247 244 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 2,  "gaussianSigmaMicrons": 1.5,  "thresholdStain1": 0.05,  "thresholdStain2": 0.2,  "addSummaryMeasurements": true}');

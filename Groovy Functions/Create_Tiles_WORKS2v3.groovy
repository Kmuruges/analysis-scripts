import qupath.lib.gui.ImageWriterTools
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QPEx
import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP

def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()

def filename = server.getShortServerName()

setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.90462 0.40605 0.12955 ", "Stain 2" : "DAB", "Values 2" : "0.49352 0.72756 0.47655 ", "Background" : " 255 255 255 "}');

selectAnnotations();
//Conversion is .2431 between Pixels to microns.  TileSizeMicrons is value squared
runPlugin('qupath.lib.algorithms.TilerPlugin', '{"tileSizeMicrons": 36.465,  "trimToROI": true,  "makeAnnotations": true,  "removeParentAnnotation": true}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 1,  "gaussianSigmaMicrons": 1.0,  "thresholdStain1": 0.2,  "thresholdStain2": 0.5,  "addSummaryMeasurements": true}');

String Imagename = getProjectEntry().getImageName()

def path = "G:\\Studies\\Compass Artifacts\\Coverslip\\"
// Make all of the paths where tiles will go, add paths and create directories accordingly
def path3 = QPEx.buildFilePath(path, "Negative150")
QPEx.mkdirs(path3)

//loop through detections
for (annotation in getAnnotationObjects()) 
{
i = 0
       //Look at all detections within a SPECIFIED annotation, not the WHOLE annotation...only commands I could get to work.
    def TumorDetections = getDetectionObjects().findAll {annotation.getROI().contains(it.getROI().getCentroidX(),it.getROI().getCentroidY() ) && it.getPathClass().getName() == "Positive"}
     
     
     for (detection in TumorDetections)
         {
             i++
             }
             print(i)
    //make roi the annotation, then find the centroids of that annotation to become the file name eventually
    roi = annotation.getROI()
    int cx = roi.getCentroidX()
    int cy = roi.getCentroidY()
    def request = RegionRequest.createInstance(imageData.getServerPath(), 
        1, roi)
    
//string for the file name, has the x and y coodinates we pulled from the annotation cx,cy
String tilename = String.format("%s %s_%s.jpg",Imagename, cx,cy)
//these series of if statements just toss a tile into a certain folder in path with the coordinates depending on whatever ratios you want

        
                                    
   if (i == 0 || i == 1){
        def dir = new File(path3)
        ImageWriterTools.writeImageRegion(server, request, path3 + "/" + tilename);
        print("wrote " + tilename)
                                    }

                                

}
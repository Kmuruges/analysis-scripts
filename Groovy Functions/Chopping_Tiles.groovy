
setImageType('BRIGHTFIELD_H_E');
setColorDeconvolutionStains('{"Name" : "H&E default", "Stain 1" : "Hematoxylin", "Values 1" : "0.65111 0.70119 0.29049 ", "Stain 2" : "Eosin", "Values 2" : "0.2159 0.8012 0.5581 ", "Background" : " 255 255 255 "}');
createSelectAllObject(true);
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.62849 0.71646 0.30279 ", "Stain 2" : "DAB", "Values 2" : "0.50932 0.80003 0.31708 ", "Background" : " 255 255 255 "}');
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "backgroundRadius": 0.0,  "medianRadius": 0.0,  "sigma": 3.0,  "minArea": 10.0,  "maxArea": 1500.0,  "threshold": 0.8,  "maxBackground": 2.0,  "watershedPostProcess": true,  "excludeDAB": false,  "cellExpansion": 5.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');
runClassifier("G:\\Studies\\Sakura QC Check\\Testq\\classifiers\\Working.qpclassifier");

tumorClass = getPathClass("Tumor")
nTumor = 0
for (detection in getDetectionObjects()) {
    pathClass = detection.getPathClass()
    if (pathClass == tumorClass)
      nTumor++
}
print(nTumor)

stromaClass = getPathClass("Stroma")
nStroma = 0
for (detection in getDetectionObjects()) {
    pathClass = detection.getPathClass()
    if (pathClass == stromaClass)
      nStroma++
}
print(nStroma)
String TumorString = ""
String StromaString = ""

if (nStroma > 0)
    StromaString = "Stroma"
if (nTumor> 0)
    TumorString = "Tumor"
print(TumorString)
print(StromaString)

import ij.IJ
import ij.ImagePlus
import qupath.imagej.images.servers.ImagePlusServer
import qupath.imagej.images.servers.ImagePlusServerBuilder
import qupath.lib.images.servers.ImageServer
import qupath.lib.regions.RegionRequest
import qupath.lib.scripting.QP

import java.awt.image.BufferedImage

/*
 * Adjustable parameters
 */
int tileWidthPixels = 128 // Width of (final) output tile in pixels
int tileHeightPixels = tileWidthPixels // Width of (final) output tile in pixels
double downsample = 1      // Downsampling used when extracting tiles
String format = "png"       // Format of the output image - TIFF or ZIP is best for ImageJ to preserve pixel sizes
String dirOutput = "G:\\Studies\\Sakura QC Check\\Final Testq\\"     // BE SURE TO ADD AN OUTPUT DIRECTORY HERE!!!

int maxErrors = 20          // Maximum number of errors... to avoid trying something doomed forever
int minImageDimension = 20  // If a tile will have a width or height < minImageDimension, it will be skipped
                            // This is needed to avoid trying to read/write images that are too tiny to be useful (and may even cause errors)

//-------------------------------------------------------

/*
 * Processing
 */

// Check we have an output directory
if (dirOutput == null) {
    println("Be sure to set the 'dirOutput' variable!")
    return
}

// Initialize error counter
int nErrors = 0

// Get the image server
ImageServer<BufferedImage> serverOriginal = QP.getCurrentImageData().getServer()

// Get an ImagePlus server
ImagePlusServer server = ImagePlusServerBuilder.ensureImagePlusWholeSlideServer(serverOriginal)

// Ensure convert the format to a file extension
String ext
if (format.startsWith("."))
    ext = format.substring(1).toLowerCase()
else
    ext = format.toLowerCase()

// Extract useful variables
String path = server.getPath()
String serverName = serverOriginal.getShortServerName()
double tileWidth = tileWidthPixels * downsample
double tileHeight = tileHeightPixels * downsample

// Loop through the image - including z-slices (even though there's normally only one...)
int counter = 0;
for (int z = 0; z < server.nZSlices(); z++) {
    for (double y = 0; y < server.getHeight(); y += tileHeight) {

        // Compute integer y coordinates
        int yi = (int)(y)
        int y2i = (int)Math.min((int)(y + tileHeight), server.getHeight());
        int hi = y2i - yi

        // Check if we requesting a region that is too small
        if (hi / downsample < minImageDimension) {
            println("Image dimension < " + minImageDimension + " - skipping row")
            continue
        }

        for (double x = 0; x < server.getWidth(); x += tileWidth) {

            // Compute integer x coordinates
            int xi = (int)(x)
            int x2i = (int)Math.min((int)(x + tileWidth), server.getWidth());
            int wi = x2i - xi

            // Create request
            RegionRequest request = RegionRequest.createInstance(path, downsample, xi, yi, wi, hi, z, 0)

            // Check if we requesting a region that is too small
            if (wi / downsample < minImageDimension) {
                // Only print warning if we've not skipped this before
                if (y > 0)
                    println("Image dimension < " + minImageDimension + " - skipping column")
                continue
            }

            // Surround with try/catch in case the server gives us trouble
            try {
                // Read the image region
                ImagePlus imp = server.readImagePlusRegion(request).getImage(false)
                // Get a suitable file name
                String name = String.format("%s (d=%.2f, x=%d, y=%d, w=%d, h=%d, z=%d).%s", serverName, downsample, xi, yi, wi, hi, z, ext)
                // Create an output file
                File file = new File(dirOutput, StromaString + " " + TumorString + " " + name)
                // Save the image
                IJ.save(imp, file.getAbsolutePath())
                // Print progress
                counter++
                println("Written tile " + counter + " to " + file.getAbsolutePath())
            } catch (Exception e) {
                // Check if we have had a sufficient number of errors to just give up
                nErrors++;
                if (nErrors > maxErrors) {
                    println("Maximum number of errors exceeded - aborting...")
                    return
                }
                e.printStackTrace()
            }
        }
    }
}
def string = "ROI: 2.00 " + qupath.lib.common.GeneralTools.micrometerSymbol() +" per pixel: DAB:  Mean"
def string2 = "ROI: 2.00 " + qupath.lib.common.GeneralTools.micrometerSymbol() +" per pixel: OD Sum:  Mean"
println string
selectObjects { p -> ((p.getPathClass() == getPathClass("Excluded")) && (p.getMeasurementList().getMeasurementValue(string) > 0.3) && (p.getMeasurementList().getMeasurementValue(string2) < 2))}
import qupath.lib.objects.classes.PathClassFactory
//Change this name
def YourClass = "Positive"
def Class = PathClassFactory.getPathClass(YourClass)
selected = getSelectedObjects()
for (def cell in selected){
cell.setPathClass(Class)
}
fireHierarchyUpdate()
println("Done!")
import asana
import requests
import time


def create_asana_task(payload):

    #print(payload)
    client = asana.Client.access_token('0/5fc7d5cb9c063ff5b9ab91046a43bc46')

    me = client.users.me()
    #print("Hello " + me['name'])

    workspace_id = me['workspaces'][0]['id']
    all_projects = next(workspace for workspace in me['workspaces'])
    projects_asana = client.projects.find_by_workspace(all_projects['id'], iterator_type=None)

    Search = 'Currently Being Scanned'

    for i,project in enumerate(projects_asana):
        #print(project)
        if project['name'] == Search:
                project_id = project['id']
                project_index = i

    project_to_add = projects_asana[project_index]
    #print(project_index)
    #print(projects_asana[project_index])

    #print(project_id)

    #parent_task = client.tasks.create(name=Search, projects=projects_asana, workspace=workspace_id)
    #print(parent_task)
    #client.tasks.add_subtask(parent_task['id'], {'name': 'This is a subtask'})
    result = client.tasks.create_in_workspace(workspace_id,
                                                  {'name': payload,
                                                   'notes': 'Note: This is a test task created with the python-asana client.',
                                                   'projects': [project_to_add['id']]})
    #client.Tasks.create({'name': 'testing123'}, workspace_id, assignee = None, assignee_status = None, completed = None, due_on = None, followers = None, projects = projects_asana[project_index])

    #print(project_id)

    #print(projects)


    #project = client.projects.create_in_workspace(workspace_id, { 'name': 'new project' })
    #print("Created project with id: " + str(workspace_id))

def post_to_study(key,value):
    date = time.strftime("%Y-%m-%d %H:%M")

    client = asana.Client.access_token('0/5fc7d5cb9c063ff5b9ab91046a43bc46')

    me = client.users.me()
    #print("Hello " + me['name'])

    workspace_id = me['workspaces'][0]['id']
    all_projects = next(workspace for workspace in me['workspaces'])
    projects_asana = client.projects.find_by_workspace(all_projects['id'], iterator_type=None)
    # print(projects_asana)
    funnerini = client.tasks.find_by_project(project_id=20018896638537, params={'name': '18-131 Calico'})
    studies = list(funnerini)
    for name1 in enumerate(studies):
        # print(type(name1))
        study_id = name1[1]
        # print(study_id)
        # print(type(study_id))
        truncated_id = study_id['name']
        # print(truncated_id)
        realtruncated_id = truncated_id[:6]
        # print(realtruncated_id)

        if realtruncated_id == key:
            # print(study_id)
            payload_id = study_id['id']
            # print("we got here")
    payload_string = str(value) + " Slides Successfully Scanned at " + str(date) + "- Posted by AsanaBot"
    #print(payload_string)
    #print(payload_id)
    client.tasks.add_comment(payload_id, params={'text': payload_string})


def send_email(user,password, recipient, subject, body):
    user = getpass.getpass("Enter Your Username")
    password = getpass.getpass("Enter Your Password")
    TEXT = ""
    recipient = ""

    pwd = password
    FROM = user
    TO = recipient
    SUBJECT = subject
    Text = body

    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, "kmurugesan@revealbio.com", message)
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")

def encode(key, clear):
        enc = []
        for i in range(len(clear)):
            key_c = key[i % len(key)]
            enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
            enc.append(enc_c)
        return base64.urlsafe_b64encode("".join(enc))


def decode(key, enc):
        dec = []
        enc = base64.urlsafe_b64decode(enc)
        for i in range(len(enc)):
            key_c = key[i % len(key)]
            dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
            dec.append(dec_c)
        return "".join(dec)
setImageType('UNSET');
setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.26027 0.83184 0.49021 ", "Stain 2" : "DAB", "Values 2" : "-0.00812 0.23087 0.97295 ", "Background" : " 253 253 252 "}');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 245,  "requestedPixelSizeMicrons": 0.25,  "minAreaMicrons": 5000000.0,  "maxHoleAreaMicrons": 50000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": false}');
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 3,  "gaussianSigmaMicrons": 2.0,  "thresholdStain1": 0.05,  "thresholdStain2": 0.99,  "addSummaryMeasurements": true}');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.26027 0.83184 0.49021 ", "Stain 2" : "DAB", "Values 2" : "-0.00812 0.23087 0.97295 ", "Background" : " 253 253 252 "}');

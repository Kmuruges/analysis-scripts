setImageType('BRIGHTFIELD_H_DAB');
selectAnnotations();
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.43004 0.82182 0.37373 ", "Stain 2" : "DAB", "Values 2" : "0.17572 0.97455 0.13922 ", "Background" : " 255 255 255 "}');
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageBrightfield": "Hematoxylin OD",  "requestedPixelSizeMicrons": 0.233,  "backgroundRadiusMicrons": 6.0,  "medianRadiusMicrons": 0.5,  "sigmaMicrons": 1.5,  "minAreaMicrons": 10.0,  "maxAreaMicrons": 400.0,  "threshold": 0.05,  "maxBackground": 0.0,  "watershedPostProcess": true,  "excludeDAB": false,  "cellExpansionMicrons": 5.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');

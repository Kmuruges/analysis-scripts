setImageType('UNSET');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 246,  "requestedPixelSizeMicrons": 0.25,  "minAreaMicrons": 5000000.0,  "maxHoleAreaMicrons": 50000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": false}');
setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.87579 0.33511 0.34742 ", "Stain 2" : "DAB", "Values 2" : "0.98121 0.18567 0.0525 ", "Background" : " 255 255 255 "}');
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 3,  "gaussianSigmaMicrons": 1.5,  "thresholdStain1": 0.1,  "thresholdStain2": 0.7,  "addSummaryMeasurements": true}');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.87579 0.33511 0.34742 ", "Stain 2" : "DAB", "Values 2" : "0.98121 0.18567 0.0525 ", "Background" : " 255 255 255 "}');

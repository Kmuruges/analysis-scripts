setImageType('BRIGHTFIELD_H_DAB');
setColorDeconvolutionStains('{"Name" : "H-DAB default", "Stain 1" : "Hematoxylin", "Values 1" : "0.91468 0.39489 0.08608 ", "Stain 2" : "DAB", "Values 2" : "0.21136 0.84329 0.49415 ", "Background" : " 252 215 233 "}');
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', '{"threshold": 245,  "requestedPixelSizeMicrons": 0.25,  "minAreaMicrons": 1000000.0,  "maxHoleAreaMicrons": 50000.0,  "darkBackground": false,  "smoothImage": true,  "medianCleanup": true,  "dilateBoundaries": false,  "smoothCoordinates": true,  "excludeOnBoundary": false,  "singleAnnotation": false}');
selectAnnotations();
runPlugin('qupath.imagej.detect.tissue.PositivePixelCounterIJ', '{"downsampleFactor": 4,  "gaussianSigmaMicrons": 2.0,  "thresholdStain1": 0.2,  "thresholdStain2": 1.5,  "addSummaryMeasurements": true}');

setImageType('FLUORESCENCE');
createSelectAllObject(true);
selectAnnotations();
runPlugin('qupath.imagej.detect.nuclei.WatershedCellDetection', '{"detectionImageFluorescence": 3,  "requestedPixelSizeMicrons": 0.0,  "backgroundRadiusMicrons": 0.0,  "medianRadiusMicrons": 0.0,  "sigmaMicrons": 1.5,  "minAreaMicrons": 10.0,  "maxAreaMicrons": 400.0,  "threshold": 55.0,  "watershedPostProcess": true,  "cellExpansionMicrons": 4.0,  "includeNuclei": true,  "smoothBoundaries": true,  "makeMeasurements": true}');
selectDetections();
runPlugin('qupath.imagej.detect.cells.SubcellularDetection', '{"detection[Channel 1]": 50.0,  "detection[Channel 2]": 75.0,  "detection[Channel 3]": -1.0,  "doSmoothing": false,  "splitByIntensity": false,  "splitByShape": true,  "spotSizeMicrons": 1.0,  "minSpotSizeMicrons": 0.5,  "maxSpotSizeMicrons": 3.0,  "includeClusters": true}');

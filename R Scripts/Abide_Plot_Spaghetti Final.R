library(dplyr)
library(readr)
library(data.table)
library(ggplot2)

path = "G:\\Studies\\18-227 Abide\\Detections\\"
setwd(path)
filelist <- dir(path,pattern = ".txt")
Summaryframe <- data.frame()

#Combine all the data into one data frame, for summary later, add the name of the file as column 39
for(f in 1:length(filelist)){
  
  Read.Data <- read.table(filelist[f], header=T, sep="\t", fill = TRUE,na.strings = "..")
  Read.Data[is.na(Read.Data)] <- 0
  file.name <- sapply(strsplit (filelist[f], split = "midbrain"), "[", 1)
  Read.Data[39] <- file.name
  
  
  print(filelist[f])
  Summaryframe <- rbind(Summaryframe,Read.Data)
  print(filelist[f])
}

path = "G:\\Studies\\18-227 Abide\\Annotations\\"
setwd(path)
filelist <- dir(path,pattern = ".txt")
AreaDraft <- data.frame()
for(k in 1:length(filelist)){
  
  Read.Data.Area <- read.table(filelist[k], header=T, sep="\t", fill = TRUE)
  
  
 
  AreaDraft[k,1] <- sum(Read.Data.Area$Area.�m.2)/1000000
  
  k = k + 1
}

#Pleasework <- data.frame()
 
#Function to make the cell body plot, input 4 variables, out comes a plot
make.plot.CellBody <- function(SizeThreshold, SizeThreshold.Activation, DABThreshold.Activation, DABThreshold.Feasible) {
  
  
  Summaryframe[is.na(Summaryframe)] <- 0
  Cell.Body <<- subset(Summaryframe,Summaryframe$Nucleus..Hematoxylin.OD.mean >= DABThreshold.Feasible & Summaryframe$Nucleus..Area > SizeThreshold)
  Process <- subset(Summaryframe, Summaryframe$Nucleus..Hematoxylin.OD.mean > DABThreshold.Feasible & Summaryframe$Nucleus..Area < SizeThreshold)
  Activated.Cell.Body <<- subset(Cell.Body, Cell.Body$Nucleus..Hematoxylin.OD.mean > DABThreshold.Activation & Cell.Body$Nucleus..Area > SizeThreshold.Activation)
  
  
  #  SLICDAB <- data.frame()
   
  # SLICDAB <- Read.Data[,c("Nucleus..Area", "Nucleus..DAB.OD.mean")]
  #SLICDAB[3] <- SLICDAB$Nucleus..Area * SLICDAB$Nucleus..DAB.OD.mean
  
  #SLICDAB2 <- subset(SLICDAB, SLICDAB$V3 > 1)
  
  
  #name <- sapply (strsplit (filelist[f], split = ".txt"), "[", 1)
  
  Pleasework <<- Activated.Cell.Body[39]
  
  CellBodyArea <<- subset(Cell.Body, select=c('V39','Nucleus..Area'))
  x1 <<- aggregate(CellBodyArea[2], list(CellBodyArea$V39), mean)
  x2 <<- aggregate(CellBodyArea[2], list(CellBodyArea$V39), sd)
  tgc <<- summarySE(CellBodyArea, measurevar="Nucleus..Area",groupvars = "V39")
  
  fill <- "#4271AE"
  line <- "#1F3552"
  Final.Density <- data.frame()
  Final.Data <<- count(Activated.Cell.Body, c('V39'))
  
  Final.Data[1,3] <<- Final.Data[1,2]/44
  Final.Data[2,3] <<- Final.Data[2,2]/45
  Final.Data[3,3] <<- Final.Data[3,2]/61
  Final.Data[4,3] <<- Final.Data[4,2]/39
  Final.Data[5,3] <<- Final.Data[5,2]/46
  Final.Data[6,3] <<- Final.Data[6,2]/53
  Final.Data[7,3] <<- Final.Data[7,2]/55
  Final.Data[8,3] <<- Final.Data[8,2]/52
  Final.Data[9,3] <<- Final.Data[9,2]/54
  Final.Data[10,3] <<- Final.Data[10,2]/40
  Final.Data[11,3] <<- Final.Data[11,2]/54
  Final.Data[12,3] <<- Final.Data[12,2]/36
  
  #x2 <<- aggregate(Activated.Cell.Body, list(Activated.Cell.Body$V39), count())
  Salines <- data.frame()
  Salines[1,1] <- Final.Data[1,3]
  Salines[2,1] <- Final.Data[5,3]
  Salines[3,1] <- Final.Data[12,3]
  
  se<<-sd(x1$Cell..Area)
  #se <- std.error(Final.Data$V3)
  mean <- mean(Final.Data$V3)


  p8 <- ggplot(tgc, aes(x = reorder(V39,Nucleus..Area), y = Nucleus..Area))
  p8 <- p8 + geom_bar(stat = "identity",position=position_dodge(),alpha = 0.5, fill = "#4271AE") + xlab("Treatment") + ylab("Mean Cell Body Area (um^2)") +geom_errorbar(aes(ymin =Nucleus..Area - se, ymax = Nucleus..Area + se))
  return(p8)
  return(Final.Data)
  return(Activated.Cell.Body)
}  

#Same thing, input 5 variables, out comes a plot
make.plot.Process <- function(SizeThreshold, SizeThreshold.Activation, DABThreshold.Activation, DABThreshold.Feasible,ProcessThresholdSize) {
  
  
  Summaryframe[is.na(Summaryframe)] <- 0
  Cell.Body <<- subset(Summaryframe,Summaryframe$Nucleus..Hematoxylin.OD.mean >= DABThreshold.Feasible & Summaryframe$Nucleus..Area > SizeThreshold)
  Process <<- subset(Summaryframe, Summaryframe$Nucleus..Hematoxylin.OD.mean > DABThreshold.Feasible & Summaryframe$Nucleus..Area < SizeThreshold)
  Activated.Cell.Body <<- subset(Cell.Body, Cell.Body$Nucleus..Hematoxylin.OD.mean > DABThreshold.Activation & Cell.Body$Nucleus..Area > SizeThreshold.Activation)
  
  Process.Activated <<- subset(Process, Process$Nucleus..Area > 50 & Process$Nucleus..Hematoxylin.OD.mean > .3)
  
  #  SLICDAB <- data.frame()
  Intensity.Process <- subset(Process, select=c('Nucleus..Area', 'Nucleus..Hematoxylin.OD.mean', 'V39'))
  Intensity.Process[4] <- Intensity.Process$Nucleus..Area * Intensity.Process$Nucleus..Hematoxylin.OD.mean/1000000
  # SLICDAB <- Read.Data[,c("Nucleus..Area", "Nucleus..DAB.OD.mean")]
  #SLICDAB[3] <- SLICDAB$Nucleus..Area * SLICDAB$Nucleus..DAB.OD.mean
  
  #SLICDAB2 <- subset(SLICDAB, SLICDAB$V3 > 1)
  x3 <<- aggregate(Intensity.Process[4], list(Intensity.Process$V39), sum)
  #Length.Process <<- subset(Process, select=c('V39', 'Nucleus..Max.caliper'))
  #name <- sapply (strsplit (filelist[f], split = ".txt"), "[", 1)
  tgc2 <<- data.frame(ncol=10)
  tgc2 <<- summarySE(Intensity.Process, measurevar="V4",groupvars = "V39")
  tgc5 <- subset(tgc2, select = c('V39', 'se'))
  tgc5[3] <- x3$V4
  Final.Data1 <<- count(Process.Activated, c('V39'))
  
  Final.Data1[1,3] <<- Final.Data1[1,2]/44
  Final.Data1[2,3] <<- Final.Data1[2,2]/45
  Final.Data1[3,3] <<- Final.Data1[3,2]/61
  Final.Data1[4,3] <<- Final.Data1[4,2]/39
  Final.Data1[5,3] <<- Final.Data1[5,2]/46
  Final.Data1[6,3] <<- Final.Data1[6,2]/53
  Final.Data1[7,3] <<- Final.Data1[7,2]/55
  Final.Data1[8,3] <<- Final.Data1[8,2]/52
  Final.Data1[9,3] <<- Final.Data1[9,2]/54
  Final.Data1[10,3] <<- Final.Data1[10,2]/40
  Final.Data1[11,3] <<- Final.Data1[11,2]/54
  Final.Data1[12,3] <<- Final.Data1[12,2]/36
  
  Salines1 <<- data.frame()
  Salines1[1,1] <<- Final.Data1[1,3]
  Salines1[2,1] <<- Final.Data1[5,3]
  Salines1[3,1] <<- Final.Data1[12,3]
  se2 <<- sd(Salines1$V1)
  fill <- "#4271AE"
  line <- "#1F3552"
  
  p9 <- ggplot(tgc5, aes(x = reorder(V39,V3), y = V3))
  p9 <- p9 + geom_bar(stat = "identity",position=position_dodge(),alpha = 0.5, fill = "#4271AE") + xlab("Treatment") + ylab("Total Process Staining") + geom_errorbar(aes(ymin =V3 - se, ymax = V3 + se))
  return(p9)
  return(Process)
  return(Pleasework)
}  

#Function used for making percentiles of the saline sample data, if you want to bin your data differently
Make.Saline.Threshold <- function(Saline.Sample.SizeThreshold, Percentile.1, Percentile.2) {
  
  Saline.Samples <- subset(Summaryframe, Summaryframe$V39 == "1-saline "| Summaryframe$V39 == "13-saline "| Summaryframe$V39 == "7-saline ")
  Process.Saline.Samples <- subset(Saline.Samples, Saline.Samples$Nucleus..Area < Saline.Sample.SizeThreshold)
  CellBody.Saline.Samples <- subset(Saline.Samples, Saline.Samples$Nucleus..Area > Saline.Sample.SizeThreshold)
  
  ProcessThresholdSize <- quantile(Process.Saline.Samples$Nucleus..Area, c(Percentile.1))
  ProcessCellBodySize <- quantile(CellBody.Saline.Samples$Nucleus..Area, c(Percentile.2))
  return(ProcessThresholdSize)
  return(CellBodySize)
}
#Call the functions down here
#Size Threshold differentiates between how much you want to call the difference between cell bodies and processes
#SizeThreshold.Activated how you want to differentiate between activated/non activated cell bodies
#DAB Threshold.Activation is Threshold for activating cell bodies
#DAB Threshold.feasible is threshold for considering something a feasible data point (Usually 0)

#Make.SalineThreshold(Value for Difference between cell body/process in terms of area, percentile of data you want to split)
ProcessThresholdSize <- Make.Saline.Threshold(125,.90,.0)
CellBodySize <- Make.Saline.Threshold(200,.95,.9)

#make.plot(SizeThreshold = 200, SizeThreshold.Activation = 1000, DABThreshold.Activation = .8, DABThreshold.Feasible = 0)
#.3 for cell area
p8 <- make.plot.CellBody(125,0,.3,.3)
p8 

#Size Threshold is difference between process/cellbody
#ProcessThresholdSize is potentially how you want to bin processes accordingly 
#make.plot.Process <- function(SizeThreshold, SizeThreshold.Activation, DABThreshold.Activation, DABThreshold.Feasible,ProcessThresholdSize) 
p9 <-make.plot.Process(200 ,50,.3,0,132)
p9


write.csv(AreaDraft,paste("Area Su1mmary11452312",f,".csv"))




summarySE <- function(data=NULL, measurevar, groupvars=NULL, na.rm=FALSE,
                      conf.interval=.95, .drop=TRUE) {
  library(plyr)
  
  # New version of length which can handle NA's: if na.rm==T, don't count them
  length2 <- function (x, na.rm=FALSE) {
    if (na.rm) sum(!is.na(x))
    else       length(x)
  }
  
  # This does the summary. For each group's data frame, return a vector with
  # N, mean, and sd
  datac <- ddply(data, groupvars, .drop=.drop,
                 .fun = function(xx, col) {
                   c(N    = length2(xx[[col]], na.rm=na.rm),
                     mean = mean   (xx[[col]], na.rm=na.rm),
                     sd   = sd     (xx[[col]], na.rm=na.rm)
                   )
                 },
                 measurevar
  )
  
  # Rename the "mean" column    
  datac <- rename(datac, c("mean" = measurevar))
  
  datac$se <- datac$sd / sqrt(datac$N)  # Calculate standard error of the mean
  
  # Confidence interval multiplier for standard error
  # Calculate t-statistic for confidence interval: 
  # e.g., if conf.interval is .95, use .975 (above/below), and use df=N-1
  ciMult <- qt(conf.interval/2 + .5, datac$N-1)
  datac$ci <- datac$se * ciMult
  
  return(datac)
}
